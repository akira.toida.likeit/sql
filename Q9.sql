﻿
use practice;

--itemテーブルとitem_categoryテーブルを結合
--テーブルをcategory_id単位で集計する
--取得するカラムはcategory_name, item_priceの合計(total_priceという名前つける)
--total_priceの値が大きな順にレコードを並び替える


select category_name, sum(item_price) as total_price 
from item_category inner join item on item_category.category_id = item.category_id 
group by item.category_id
order by total_price desc ;
